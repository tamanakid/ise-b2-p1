-- Este modulo realiza el disparo de una captura de temperatura y humedad, escribiendo en
-- el puntero del sensor (direccion I2C x40) la direccion del registro de temperatura (x00);
-- el disparo consiste, por tanto en una escritura I2C de 2 bytes, que se realiza coincidiendo 
-- con la ocurrencia de un tic que tiene un periodo de 0.25 segundos.
-- En el siguiente tic se lee la temperatura y la humedad en una lectura I2C de 4 bytes; el valor 
-- leido de ambas magnitudes se convierte a BCD y se entrega por las salidas temp_BCD, sgn_T y humd_BCD.

--    Designer: DTE
--    Versi�n: 1.0
--    Fecha: 25-11-2016 
 
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity procesador_medida is
port(clk:         in     std_logic;
     nRst:        in     std_logic;
     nRst_led:    out    std_logic;

     -- Temporizacion
     tic_0_25s:   in     std_logic;
    
     -- Interfaz periferico I2C
     we:          buffer std_logic;
     rd:          buffer std_logic;
     add:         buffer std_logic_vector(1 downto 0);
     dato_w:      buffer std_logic_vector(7 downto 0);
     dato_r:      in     std_logic_vector(7 downto 0);

     -- Interfaz presentacion
     dato_leido:    buffer std_logic_vector(31 downto 0);

     -- Senhales de interrupcion
     int_req:	    buffer std_logic;
     int_req_led:   buffer std_logic;
     int_res:       in std_logic;
     int_res_led:   buffer std_logic
    );          

end entity;

architecture rtl of procesador_medida is
 
  type   t_estado is (espera_tic, escritura1, escritura2, escritura3,  
                      escritura4, escritura5, comando_escritura, chequeo_fin_escr);
  signal estado: t_estado;

  type t_estado_cont is (off, counting);
  signal estado_cont: t_estado_cont;
  
  signal int_res_sync: std_logic_vector(1 downto 0);


 begin
  -- Automata de prueba
  -- Realiza una operacion de escritura y una de lectura cada 0.25 segundos
  process(clk, nRst)
  begin
    if nRst = '0' then
      nRst_led <= '0';
      estado <= espera_tic;
      we <= '0';
      rd <= '0';
      add <= "11"; -- apunta al registro de configuracion/status
      dato_w <= X"00";
      int_req <= '0';
      int_req_led <= '0';

    elsif clk'event and clk = '1' then
      nRst_led <= '1';

      if int_res_sync(1) = '0' then
        int_req <= '0';
      end if;

      case(estado) is
        when espera_tic =>
          if dato_r(0) = '1' and tic_0_25s = '1' then -- bit 0 status = 1-> listo para nueva operacion (no es necesario activar rd)
            -- Escritura de la direccion del esclavo
            we <= '1'; -- escritura
            add <= "00"; -- @FIFO de salida
            dato_w <= "1000000" & '0'; -- 0x40 (dir. I2C del esclavo, cambiarla por la real) & 0 (operacion de escritura I2C)
            estado <= escritura1;
            int_req <= '0';
          end if;
        when escritura1 =>
          -- adaptar valor y n� de escrituras a la aplicacion
          dato_w <= dato_leido(31 downto 24);
          estado <= escritura2;
        when escritura2 =>
          -- adaptar valor y n� de escrituras a la aplicacion
          dato_w <= dato_leido(23 downto 16);
          estado <= escritura3;
        when escritura3 =>
          -- adaptar valor y n� de escrituras a la aplicacion
          dato_w <= dato_leido(15 downto 8);
          estado <= escritura4;
        when escritura4 =>
          -- adaptar valor y n� de escrituras a la aplicacion
          dato_w <= dato_leido(7 downto 0);
          estado <= escritura5;
        when escritura5 =>
          add <= "11"; -- apunta al registro de configuracion/status
          dato_w <= x"05"; -- start transaction
          estado <= comando_escritura;
        when comando_escritura =>
          we <= '0';
          estado <= chequeo_fin_escr;
          int_req <= '1';
          int_req_led <= not(int_req_led);
        when chequeo_fin_escr =>
          if dato_r(0) = '1' then -- bit 0 status = 1-> listo para nueva operacion
            estado <= espera_tic;
          end if;
      end case;
    end if;
  end process;


  -- Sincronizacion de int_res con el reloj del sistema (Prevencion de metaestabilidad)
  process(clk, nRst)
  begin
    if nRst = '0' then
      int_res_sync <= "00";
      int_res_led <= '0';

    elsif clk'event and clk = '1' then
      int_res_sync <= int_res_sync(0) & int_res;
      int_res_led <= int_res_sync(0);
    end if;
  end process;


  -- Contador 32 bits
  process(clk, nRst)
  begin
    if nRst = '0' then
      dato_leido <= (others => '0');
      estado_cont <= off;

    elsif clk'event and clk = '1' then
      case (estado_cont) is
        when off =>
          if int_req = '1' then
            estado_cont <= counting;
            dato_leido <= (others => '0');
          end if;

        when counting =>
          if int_req = '1' then
            dato_leido <= dato_leido + 1;
          else
            estado_cont <= off;
          end if;
      end case;
    end if;
  end process;
end rtl;