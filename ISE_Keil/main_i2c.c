/* PINS (Pinmode 2)
 * 28: SDA2 P0.10
 * 27: SCL2 P0.11
 */

#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "Driver_I2C.h"
// #include "I2C_LPC17xx.h"

/* h file */
#define I2C_SLAVE_ADDR    0x40

extern ARM_DRIVER_I2C Driver_I2C2;
// extern ARM_DRIVER_I2C *i2c_drv;
/* end h file */

ARM_DRIVER_I2C *i2c_drv = &Driver_I2C2;

#define PORT_LED 		1
#define PIN_LED0 		18
#define PIN_LED1 		20
#define PIN_LED2 		21
#define PIN_LED3 		23

/* AppBoard RGB LED Constants */
#define PORT_RGB        2
#define PIN_RED         3
#define PIN_GREEN       2
#define PIN_BLUE        1

#define PORT_JST		0
#define PIN_JST_D		17




/* Utils */

void hard_delay (int cycles) {
	int i;
	for (i = 0; i < cycles; i++);
}

void gpio_initialize(void) {
	/* LEDs */
	GPIO_SetDir(PORT_LED, PIN_LED0, GPIO_DIR_OUTPUT);
	GPIO_SetDir(PORT_LED, PIN_LED1, GPIO_DIR_OUTPUT);
	GPIO_SetDir(PORT_LED, PIN_LED2, GPIO_DIR_OUTPUT);
	GPIO_SetDir(PORT_LED, PIN_LED3, GPIO_DIR_OUTPUT);

	/* RGB_LED */
	GPIO_SetDir(PORT_RGB, PIN_GREEN, GPIO_DIR_OUTPUT);
	GPIO_SetDir(PORT_RGB, PIN_RED, GPIO_DIR_OUTPUT);
	GPIO_SetDir(PORT_RGB, PIN_BLUE, GPIO_DIR_OUTPUT);
	GPIO_PinWrite (PORT_RGB, PIN_RED, 0);
	GPIO_PinWrite (PORT_RGB, PIN_GREEN, 1);
	GPIO_PinWrite (PORT_RGB, PIN_BLUE, 1);
	
	/* Joystick Down */
	PIN_Configure(PORT_JST, PIN_JST_D, PIN_FUNC_0, PIN_PINMODE_PULLDOWN, PIN_PINMODE_NORMAL); // Might be better as pullup
	LPC_GPIOINT->IO0IntEnR |= (1 << PIN_JST_D);
	
	NVIC_EnableIRQ(EINT3_IRQn);
	
	/* nRst */
	PIN_Configure(PORT_RGB, PIN_RED, PIN_FUNC_0, PIN_PINMODE_PULLUP, PIN_PINMODE_NORMAL);
	GPIO_PinWrite(PORT_RGB, PIN_RED, 1);
	hard_delay(100);

}

void leds_write() {
	static int pin0 = 0;
	static int pin1 = 0;
	static int pin2 = 0;
	static int pin3 = 0;
	pin0 = pin0 == 0 ? 1 : 0;
	pin1 = pin1 == 0 ? 1 : 0;
	pin2 = pin2 == 0 ? 1 : 0;
	pin3 = pin3 == 0 ? 1 : 0;
	GPIO_PinWrite(PORT_LED, PIN_LED0, pin0);
	GPIO_PinWrite(PORT_LED, PIN_LED1, pin1);
	GPIO_PinWrite(PORT_LED, PIN_LED2, pin2);
	GPIO_PinWrite(PORT_LED, PIN_LED3, pin3);
}




/** MASTER */

void i2c_master_initialize () {
  i2c_drv->Initialize(NULL); /* If "pooling" is true, argument should be I2C_SignalEvent */
  i2c_drv->PowerControl(ARM_POWER_FULL);
  i2c_drv->Control(ARM_I2C_BUS_SPEED, ARM_I2C_BUS_SPEED_FAST);
  i2c_drv->Control(ARM_I2C_BUS_CLEAR, 0);
}


void i2c_master_transmit () {
  int transferred;
  bool generate_stop = false;
  uint8_t buffer_tx[4] = { 0x11, 0x22, 0x33, 0x44 };
  
  i2c_drv->MasterTransmit(I2C_SLAVE_ADDR, buffer_tx, 4, generate_stop);
  while (i2c_drv->GetStatus().busy);
  
  transferred = i2c_drv->GetDataCount();
  if (transferred != 4) {
    transferred = 0;
  };
}


void i2c_master_deploy() {
  i2c_master_initialize();
  i2c_master_transmit ();
}



/** SLAVE */

static volatile uint32_t I2C_Event;

/* I2C Signal Event function callback */
static void I2C_SignalEvent (uint32_t event) {
  I2C_Event |= event;
}


void i2c_slave_initialize () {
  /* Initialize I2C peripheral */
  i2c_drv->Initialize(I2C_SignalEvent); /*I2C_SignalEvent*/
 
  /* Power-on I2C peripheral */
  i2c_drv->PowerControl(ARM_POWER_FULL);
 
  /* Configure I2C bus */
  i2c_drv->Control(ARM_I2C_OWN_ADDRESS, I2C_SLAVE_ADDR);
}


void i2c_slave_listen () {
  uint8_t buffer_rx[4];
  
  /* Receive chunk */
  i2c_drv->SlaveReceive(buffer_rx, 4);
  while ((I2C_Event & ARM_I2C_EVENT_TRANSFER_DONE) == 0);
  /* Clear transfer done flag */
  I2C_Event &= ~ARM_I2C_EVENT_TRANSFER_DONE;
	
	leds_write();
}


void i2c_slave_deploy() {
	gpio_initialize();
  i2c_slave_initialize();
  while (1) {
    i2c_slave_listen ();
  }
}



/** MAIN */

int main () {
  // i2c_master_deploy();
  i2c_slave_deploy();
}



/**
 * GPIO Interrupt Service Routine
 */
int led_green = 1;
void EINT3_IRQHandler() {
	uint32_t statusR = LPC_GPIOINT->IO0IntStatR;

	/* Rising Edges */
  if (statusR & (1 << PIN_JST_D)) {
    LPC_GPIOINT->IO0IntClr |= (1 << PIN_JST_D);
		led_green = led_green == 1 ? 0 : 1;
		GPIO_PinWrite (PORT_RGB, PIN_GREEN, led_green);
  }
}

